from django import forms
from .models import Receipt, ExpenseCategory, Account

# meta is a needed class on all forms (for now) - use in final


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]


class ExpenseCreateForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class AccountCreateForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
