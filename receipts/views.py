from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, render

# importing my models
from receipts.models import ExpenseCategory, Receipt, Account

# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    # If experiencing trouble with this view and displaying
    # the CONTEXT for the template,
    # may want to switch to function Listview
    template_name = "list.html"  # placeholder for when I name
    context_object_name = "receiptlist"


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")

    # def form_invalid(self, form):
    #     print(form)


#  the other two lists (expense and account)

#  EXPENSE
class ExpenseListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expenselist"
    context_object_name = "ExpenseList"

    #  use get_queryset to add logic to the list selection
    def get_queryset(self):
        queryset = ExpenseCategory.objects.filter(owner=self.request.user)
        for item in queryset:
            print(item)
        return queryset


class ExpenseCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/expensecreate.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("categories_list")


#  ACCOUNT


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accountlist"
    context_object_name = "AccountList"

    #  use get_queryset to add logic to the list selection
    def get_queryset(self):
        queryset = Account.objects.filter(owner=self.request.user)
        return queryset


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/create_account.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_list")