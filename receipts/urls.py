from django.urls import path


# import my views
from receipts.views import (
    ReceiptListView,
    AccountListView,
    ReceiptCreateView,
    ExpenseListView,
    ExpenseCreateView,
    AccountCreateView,
)


# create URL patterns

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/", ExpenseListView.as_view(), name="categories_list"),
    path(
        "categories/create/",
        ExpenseCreateView.as_view(),
        name="create_category",
    ),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "accounts/create", AccountCreateView.as_view(), name="create_account"
    ),
]
