from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.urls import is_valid_path
from django.contrib.auth.models import User

# Create your views here.


def login_page(request):

    return render(request, "accounts/login.html")  # check the name if bugged


def signup(request):
    if request.method != "POST":
        form = UserCreationForm()
    else:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.instance
            print(user)
            form.save()
            return redirect("home")

    context = {"form": form}

    return render(request, "registration/signup.html", context)
